﻿namespace ReportedIssuesApi.Models
{
    public class LoginDataModel
    {
        private int _loginID;
        private int _userID;
        private string _password;

        public LoginDataModel(int loginID, int userID, string password)
        {
            _loginID = loginID;
            _userID = userID;
            _password = password;
        }

        public LoginDataModel()
        {

        }

        public int LoginID { get { return _loginID; } set { _loginID = value; } }
        public int UserID { get { return _userID; } set { _userID = value; } }
        public string Password { get { return _password; } set { _password = value; } }
    }
}