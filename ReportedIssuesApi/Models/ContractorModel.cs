﻿using System.Collections.Generic;

namespace ReportedIssuesApi.Models
{
    public class ContractorModel
    {
        private int _contractorID;
        private string _companyName;
        private string _lineOfBusiness;

        public ContractorModel()
        {
            
        }

        public ContractorModel(int contractorID, string companyName, string lineOfBusiness)
        {
            _contractorID = contractorID;
            _companyName = companyName;
            _lineOfBusiness = lineOfBusiness;
        }
        public int ContractorID { get { return _contractorID; } set { _contractorID = value; } }
        public string CompanyName { get { return _companyName; } set { _companyName = value; } }
        public string LineOfBusiness { get { return _lineOfBusiness; } set { _lineOfBusiness = value; } }
    }
}