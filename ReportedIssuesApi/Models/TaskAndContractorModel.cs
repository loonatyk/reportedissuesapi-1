﻿namespace ReportedIssuesApi.Models
{
    public class TaskAndContractorModel
    {
        private int _taskAndContractorID;
        private int _taskID;
        private int _contractorID;

        public TaskAndContractorModel(int taskAndContractorId, int taskID, int contractorID)
        {
            _taskAndContractorID = taskAndContractorId;
            _taskID = taskID;
            _contractorID = contractorID;
        }

        public TaskAndContractorModel()
        {

        }

        public int TaskAndContractorID { get { return _taskAndContractorID; } set { _taskAndContractorID = value; } }
        public int TaskID { get { return _taskID; } set { _taskID = value; } }
        public int ContractorID { get { return _contractorID; } set { _contractorID = value; } }

        //public virtual ContractorModel ContractorModel { get; set; }
        //public virtual TaskModel TaskModel { get; set; }
    }
}