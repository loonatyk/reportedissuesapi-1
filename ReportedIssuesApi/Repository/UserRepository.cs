﻿using ReportedIssuesApi.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace ReportedIssuesApi.Repository
{
    public class UserRepository
    {

        //ALL
        public async Task<List<UserModel>> GetAllUsers()
        {
            using (var context = new ReportedIssuesEntities())
            {
                var users = context.Users
                    .Select(x => new UserModel()
                    {
                        UserID = x.UserID,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Role = x.Role,
                        Email = x.Email

                    })
                .ToList<UserModel>();
                if (users.Count != 0)
                {
                    return users;
                }
                else
                {
                    return null;
                }
            }
        }

        //BY ID
        public async Task<List<UserModel>> GetUserByID(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var user = context.Users
                    .Where(x => x.UserID == id)
                    .Select(x => new UserModel()
                    {
                        UserID = x.UserID,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Role = x.Role,
                        Email = x.Email
                    })
                    .ToList<UserModel>();
                if (user.Count != 0)
                {
                    return user;
                }
                else
                {
                    return null;
                }

            }
        }

        //BY Role
        public async Task<List<UserModel>> GetUserByRole(string role)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var users = context.Users
                    .Where(x => x.Role == role)
                    .Select(x => new UserModel()
                    {
                        UserID = x.UserID,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Role = x.Role,
                        Email = x.Email
                    })
                    .ToList<UserModel>();
                if (users.Count != 0)
                {
                    return users;
                }
                else
                {
                    return null;
                }
            }
        }

        //BY NAME&LastName
        public async Task<List<UserModel>> GetUserByNameAndLastName(string firstName, string lastName)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var user = context.Users
                    .Where(x => x.FirstName == firstName && x.LastName == lastName)
                    .Select(x => new UserModel()
                    {
                        UserID = x.UserID,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Role = x.Role,
                        Email = x.Email
                    })
                    .ToList<UserModel>();
                if (user.Count != 0)
                {
                    return user;
                }
                else
                {
                    return null;
                }

            }
        }


        public async Task<bool> UpdateUser(UserModel updatedUser)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var user = context.Users
                            .Where(x => x.UserID == updatedUser.UserID)
                            .FirstOrDefault();
                if (user != null)
                {
                    user.FirstName = updatedUser.FirstName;
                    user.LastName = updatedUser.LastName;
                    user.Role = updatedUser.Role;
                    user.Email = updatedUser.Email;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //INSERT
        User user = new User();
        public async Task<bool> InsertUser(UserModel newUser)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var email = context.Users.Where(x => x.Email == newUser.Email)
                    .Select(x => new UserModel()
                    {
                        UserID = x.UserID
                    }).ToList<UserModel>();

                user.FirstName = newUser.FirstName;
                user.LastName = newUser.LastName;
                user.Role = newUser.Role;
                user.Email = newUser.Email;
                if (newUser != null && email.Count == 0)
                {
                    context.Users.Add(user);
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //DELETE
        public async Task<bool> DeleteUser(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var user = context.Users
                    .FirstOrDefault(x => x.UserID == id);

                if (user != null)
                {
                    context.Users.Remove(user);
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


    }
}