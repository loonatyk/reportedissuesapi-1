﻿using ReportedIssuesApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportedIssuesApi.Repository
{
    public class TaskAndContractorRepository
    {
        //Models.Task insertedTask = new Models.Task();
        //Models.Contractor insertedContractor = new Models.Contractor();

        public async Task<List<TaskAndContractorModel>> GetAllTaskAndContractor()
        {
            using (var context = new ReportedIssuesEntities())
            {
                var all = context.TaskAndContractors
                    .Select(x => new TaskAndContractorModel
                    {
                        TaskAndContractorID = x.TaskAndContractorsID,
                        TaskID = x.TaskID,
                        ContractorID = x.ContractorID
                    }).ToList<TaskAndContractorModel>();

                if (all.Count == 0)
                {
                    return null;
                }
                else
                {
                    return all;
                }
            }
        }
        public async Task<List<TaskAndContractorModel>> GetAllByTask(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var all = context.TaskAndContractors
                    .Where(x=>x.TaskID==id)
                    .Select(x => new TaskAndContractorModel
                    {
                        TaskAndContractorID = x.TaskAndContractorsID,
                        TaskID = x.TaskID,
                        ContractorID = x.ContractorID
                    }).ToList<TaskAndContractorModel>();

                if (all.Count == 0)
                {
                    return null;
                }
                else
                {
                    return all;
                }
            }
        }
        public async Task<List<TaskAndContractorModel>> GetAllByContractor(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var all = context.TaskAndContractors
                    .Where(x => x.ContractorID == id)
                    .Select(x => new TaskAndContractorModel
                    {
                        TaskAndContractorID = x.TaskAndContractorsID,
                        TaskID = x.TaskID,
                        ContractorID = x.ContractorID
                    }).ToList<TaskAndContractorModel>();

                if (all.Count == 0)
                {
                    return null;
                }
                else
                {
                    return all;
                }
            }
        }

        public async Task<bool> InsertTaskAndContractor(TaskAndContractorModel taskAndContractor)
        {
            if (taskAndContractor == null)
            {
                return false;
            }

            using (var context = new ReportedIssuesEntities())
            {
                var task = context.Tasks.Where(x => x.TaskID == taskAndContractor.TaskID)
                    .FirstOrDefault();
                //.Select(x => new TaskModel()
                //{
                //    TaskID = x.TaskID
                //}).ToList<TaskModel>();
                var contractor = context.Contractors.Where(x => x.ContractorID == taskAndContractor.ContractorID)
                    .FirstOrDefault();
                //.Select(x => new ContractorModel()
                //{
                //    ContractorID = x.ContractorID
                //}).ToList<ContractorModel>();

                if (task == null || contractor == null)
                {
                    return false;
                }
                else
                {
                    TaskAndContractor taskandcontractor = new TaskAndContractor();
                    taskandcontractor.TaskID = taskAndContractor.TaskID;
                    context.TaskAndContractors.Add(taskandcontractor);
                    taskandcontractor.ContractorID = taskAndContractor.ContractorID;
                    context.TaskAndContractors.Add(taskandcontractor);
                    context.SaveChanges();
                    return true;
                }
            }
        }

        //public async Task<bool> Insert(Models.Task task, Contractor contractor)
        //{
        //    using (var context = new ReportedIssuesEntities())
        //    {
        //        TaskAndContractor tac = new TaskAndContractor();
        //        tac.TaskID = task.TaskID;
        //        context.TaskAndContractors.Add(tac);
        //        tac.ContractorID = contractor.ContractorID;
        //        context.TaskAndContractors.Add(tac);
        //        context.SaveChanges();
        //        return true;
        //    }
        //}

        public async Task<bool> UpdateTaskAndContractor(TaskAndContractorModel taskAndContractor)
        {
            if (taskAndContractor == null)
            {
                return false;
            }

            using (var context = new ReportedIssuesEntities())
            {
                var IsExist = context.TaskAndContractors
                    .Where(x => x.TaskID == taskAndContractor.TaskID && x.ContractorID == taskAndContractor.ContractorID)
                    .Select(x => new TaskAndContractor()
                    {
                        TaskAndContractorsID = x.TaskAndContractorsID
                    }).ToList();
                if (IsExist == null)
                {
                    var task = context.TaskAndContractors
                   .FirstOrDefault(x => x.TaskAndContractorsID == taskAndContractor.TaskAndContractorID);
                    task.TaskID = taskAndContractor.TaskID;
                    task.ContractorID = taskAndContractor.ContractorID;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public async Task<bool> DeleteTaskAndContractor(int id)
        {
            using (var context = new ReportedIssuesEntities())
            {
                var task = context.TaskAndContractors
                    .FirstOrDefault(x => x.TaskAndContractorsID == id);

                if (task != null)
                {
                    context.TaskAndContractors.Remove(task);
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}