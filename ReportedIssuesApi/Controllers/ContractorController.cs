﻿using ReportedIssuesApi.Models;
using ReportedIssuesApi.Repository;
using System.Threading.Tasks;
using System.Web.Http;

namespace ReportedIssuesApi.Controllers
{
    public class ContractorController : ApiController
    {
        private readonly ContractorRepository contractorRepo = new ContractorRepository();

        // GET api/contractor
        [Route("api/contractor")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllContractors()
        {
            var contractors = await contractorRepo.GetAllContractors();
            if (contractors!=null)
            {
                return Ok(contractors);
            }
            else
            {
                return BadRequest("No Contractors found!");
            }
        }

        // api/contractor/2
        [Route("api/contractor/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetContractorById(int id)
        {
            var contractor = await contractorRepo.GetContractorByID(id);
            if (contractor!=null)
            {
                return Ok(contractor);
            }
            else
            {
                return BadRequest("Contractor with given id does not exist");
            }
        }

        // api/contractor/new
        [Route("api/contractor/new")]
        [HttpPost]
        public async Task<IHttpActionResult> NewContractor([FromBody] ContractorModel contr)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }

            var newContractor = await contractorRepo.InsertContractor(contr);
            
            if (!newContractor)
            {
                return BadRequest("Contractor with given name already exists");
            }
            
            return Ok("Contractor added successfully");
            
        }

        // api/contractor/update
        [Route("api/contractor/update")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateContractor([FromBody] ContractorModel contr)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }
            
            var updateContractor = await contractorRepo.UpdateContractor(contr);

            if (updateContractor == ContractorRepository.UpdateContractorResult.ContractorDoesNotExist)
            {
                return BadRequest("Contractor with given id does not exist");
            }
            if (updateContractor == ContractorRepository.UpdateContractorResult.ContractorAlreadyExists)
            {
                return BadRequest("Contractor with given name already exists");
            }
            
            return Ok("Contractor updated successfully");
            
        }

        // api/contractor/remove/2
        [Route("api/contractor/remove/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveContractor(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }
            else
            {
                await contractorRepo.DeleteContractor(id);
                return Ok("Contractor removed successfully");
            }
        }
    }
}
