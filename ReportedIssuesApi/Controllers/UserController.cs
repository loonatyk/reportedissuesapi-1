﻿using System.Threading.Tasks;
using System.Web.Http;
using ReportedIssuesApi.Models;
using ReportedIssuesApi.Repository;

namespace ReportedIssuesApi.Controllers
{
    public class UserController : ApiController
    {
        private readonly UserRepository _userRepo = new UserRepository();

        [Route("api/user")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAll()
        {
            var users = await _userRepo.GetAllUsers();
            if (users != null)
            {
                return Ok(users);
            }
            else
            {
                return BadRequest("No users found");
            }
        }

        [Route("api/user/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserByID(int id)
        {
            var user = await _userRepo.GetUserByID(id);
            if (user != null)
            {
                return Ok(user);
            }
            else
            {
                return BadRequest("User with given id does not exist");
            }
        }

        [Route("api/user/role/{role}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserByRole(string role)
        {
            var user = await _userRepo.GetUserByRole(role);
            if(user != null)
            {
                return Ok(user);
            }
            else
            {
                return BadRequest("User(s) with given role do(es) not exist");
            }
        }
        [Route("api/user/{firstname}/{lastname}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserByName(string firstname, string lastname)
        {
            var user = await _userRepo.GetUserByNameAndLastName(firstname, lastname);
            if (user != null)
            {
                return Ok(user);
            }
            else
            {
                return BadRequest("User(s) with given name do(es) not exist");
            }
        }

        [Route("api/user")]
        [HttpPost]
        public async Task<IHttpActionResult> InsertUser(UserModel newUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }

            var newUserResult = await _userRepo.InsertUser(newUser);

            if (!newUserResult)
            {
                return BadRequest("Email already registered!");
            }
            else
            {
                return Ok("User added successfuly");
            }
        }

        [Route("api/user/{id}")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateUser(int id, UserModel updatedUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model");
            }

            var result = await _userRepo.UpdateUser(updatedUser);

            if (!result)
            {
                return BadRequest("User with given id does not exit");
            }

            return Ok("User updated sucessfully");
        }

        [Route("api/user/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteUser(int id)
        {
            var user = await _userRepo.DeleteUser(id);
            if (user == true)
            {
                return Ok("User removed successfully");
            }
            else
            {
                return BadRequest("User with given id does not exit");
            }
        }

    }
}
